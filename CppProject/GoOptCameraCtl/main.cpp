﻿// GoOptCameraCtl.cpp : Defines the entry point for the application.
//

#include <Windows.h>

#include <iostream>

#include "GoOptCameraCtl.h"
#include "normalSDK.h"

using namespace std;

void main() {
  int32_t isGrabbingFlag = 0;

  int32_t ret;
  GENICAM_System *pSystem = NULL;
  GENICAM_Camera *pCamera = NULL;
  GENICAM_Camera *pCameraList = NULL;
  GENICAM_StreamSource *pStreamSource = NULL;
  GENICAM_AcquisitionControl *pAcquisitionCtrl = NULL;
  GENICAM_AcquisitionControlInfo acquisitionControlInfo = {0};
  uint32_t cameraCnt = 0;
  int cameraIndex = -1;

  // discover camera
  //发现设备
  ret = GENICAM_getSystemInstance(&pSystem);
  if (-1 == ret) {
    printf("pSystem is null.\r\n");
    getchar();
    return;
  }

  ret = pSystem->discovery(pSystem, &pCameraList, &cameraCnt, typeAll);
  if (-1 == ret) {
    printf("discovery device fail.\r\n");
    getchar();
    return;
  }

  if (cameraCnt < 1) {
    printf("there is no device.\r\n");
    getchar();
    return;
  }

  // print camera info (index,Type,vendor name, model,serial
  // number,DeviceUserID,IP Address)
  // 打印相机基本信息（序号,类型,制造商信息,型号,序列号,用户自定义ID,IP地址）
  displayDeviceInfo(pCameraList, cameraCnt);
  // Select the camera
  // 选择需要连接的相机
  cameraIndex = selectDevice(cameraCnt);
  pCamera = &pCameraList[cameraIndex];

  // connect to camera
  //连接设备
  ret = GENICAM_connect(pCamera);
  if (ret != 0) {
    printf("connect camera failed.\n");
    getchar();
    return;
  }

  //创建属性节点 AcquisitionControl
  acquisitionControlInfo.pCamera = pCamera;
  ret = GENICAM_createAcquisitionControl(&acquisitionControlInfo,
                                         &pAcquisitionCtrl);
  if (ret != 0) {
    printf("Create Acquisition Control Fail.\n");
    getchar();
    return;
  }

  // Set Soft Trigger Config
  //设置软触发配置
  ret = setSoftTriggerConf(pAcquisitionCtrl);
  if (ret != 0) {
    printf("set soft trigger config failed.\n");
    //注意：需要释放pAcquisitionCtrl内部对象内存
    pAcquisitionCtrl->release(pAcquisitionCtrl);
    getchar();
    return;
  }

  // create stream source instance
  //创建流对象
  ret = GENICAM_CreateStreamSource(pCamera, &pStreamSource);
  if ((ret != 0) || (NULL == pStreamSource)) {
    printf("create stream obj  fail.\r\n");
    //注意：需要释放pAcquisitionCtrl内部对象内存
    pAcquisitionCtrl->release(pAcquisitionCtrl);
    getchar();
    return;
  }

  //注册回调函数
  ret = pStreamSource->attachGrabbing(pStreamSource, onGetFrame);
  if (ret != 0) {
    printf("attch Grabbing fail!\n");
    //注意：需要释放pAcquisitionCtrl内部对象内存
    pAcquisitionCtrl->release(pAcquisitionCtrl);

    //注意：需要释放pStreamSource内部对象内存
    pStreamSource->release(pStreamSource);
    getchar();
    return;
  }

  // start grabbing from camera
  //开始抓流
  ret = GENICAM_startGrabbing(pStreamSource);
  if (ret != 0) {
    printf("StartGrabbing  fail.\n");
    isGrabbingFlag = 0;
    //注意：需要释放pAcquisitionCtrl内部对象内存
    pAcquisitionCtrl->release(pAcquisitionCtrl);

    //注意：需要释放pStreamSource内部对象内存
    pStreamSource->release(pStreamSource);
    getchar();
    return;
  } else {
    isGrabbingFlag = 1;
  }

  //执行一次软触发
  ret = executeTriggerSoftware(pAcquisitionCtrl);
  if (ret != 0) {
    printf("TriggerSoftware fail.\n");
    //注意：需要释放pAcquisitionCtrl内部对象内存
    pAcquisitionCtrl->release(pAcquisitionCtrl);

    //注意：需要释放pStreamSource内部对象内存
    pStreamSource->release(pStreamSource);
    getchar();
    return;
  }

  //注意：需要释放pAcquisitionCtrl内部对象内存
  pAcquisitionCtrl->release(pAcquisitionCtrl);

  while (isGrabbingFlag) {
    Sleep(50);
  }

  //注销回调函数
  ret = pStreamSource->detachGrabbing(pStreamSource, onGetFrame);
  if (ret != 0) {
    printf("detachGrabbing  fail.\n");
  }

  // stop grabbing from camera
  //停止抓流
  ret = GENICAM_stopGrabbing(pStreamSource);
  if (ret != 0) {
    printf("Stop Grabbing  fail.\n");
  }

  //注意：需要释放pStreamSource内部对象内存
  pStreamSource->release(pStreamSource);

  // disconnect camera
  //断开设备
  ret = GENICAM_disconnect(pCamera);
  if (ret != 0) {
    printf("disconnect camera failed.\n");
    getchar();
    return;
  }

  printf("Press any key to exit.\n");
  getchar();
  return;
}
