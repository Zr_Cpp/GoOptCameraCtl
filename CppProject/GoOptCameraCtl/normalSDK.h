#pragma once

#include "SDK.h"

int32_t GENICAM_connect(GENICAM_Camera *pGetCamera);
int32_t GENICAM_CreateStreamSource(GENICAM_Camera *pGetCamera, GENICAM_StreamSource **ppStreamSource);
int32_t GENICAM_startGrabbing(GENICAM_StreamSource *pStreamSource);
int32_t GENICAM_stopGrabbing(GENICAM_StreamSource *pStreamSource);
int32_t setSoftTriggerConf(GENICAM_AcquisitionControl *pAcquisitionCtrl);
int32_t executeTriggerSoftware(GENICAM_AcquisitionControl *pAcquisitionCtrl);
int32_t GENICAM_disconnect(GENICAM_Camera *pGetCamera);
void onGetFrame(GENICAM_Frame* pFrame);
void displayDeviceInfo(GENICAM_Camera *pCameraList, int cameraCnt);
char* trim(char* pStr);
int isInputValid(char* pInpuStr);
int selectDevice(int cameraCnt);