﻿/// \file
/// \~chinese
/// \brief 软触发范例
/// \example main.c
/// \~english
/// \brief soft trigger sample
/// \example main.c

/************************************************************************/
/* This Demo show how to use GenICam API(C) to write a simple code.
   Please make sure the camera and PC are in the same network(IP address is correctly seted) before run the demo. */

/* 本Demo为简单演示SDK的使用，没有附加修改相机IP的代码，在运行之前，请使
	用相机客户端修改相机IP地址的网段与主机的网段一致                 */
/*																		 */
/* 本工程只演示发现设备，连接设备，设置软触发，软触发，断开连接        */
/************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <Windows.h>
#include "normalSDK.h"

static int32_t isGrabbingFlag=0;

 int32_t GENICAM_connect(GENICAM_Camera *pGetCamera)
{
	int32_t isConnectSuccess;

	isConnectSuccess = pGetCamera->connect(pGetCamera, accessPermissionControl);
	if( isConnectSuccess != 0)
	{
		printf("connect camera failed.\n");
		return -1;
	}
	
	return 0;
}

 int32_t GENICAM_CreateStreamSource(GENICAM_Camera *pGetCamera, GENICAM_StreamSource **ppStreamSource)
{
	int32_t isCreateStreamSource;
	GENICAM_StreamSourceInfo stStreamSourceInfo;


	stStreamSourceInfo.channelId = 0;
	stStreamSourceInfo.pCamera = pGetCamera;

	isCreateStreamSource = GENICAM_createStreamSource(&stStreamSourceInfo, ppStreamSource);

	if( isCreateStreamSource != 0)
	{
		printf("create stream obj  fail.\r\n");
		return -1;
	}

	return 0;
}

 int32_t GENICAM_startGrabbing(GENICAM_StreamSource *pStreamSource)
{
	int32_t isStartGrabbingSuccess;
	GENICAM_EGrabStrategy eGrabStrategy;

	eGrabStrategy = grabStrartegySequential;
	isStartGrabbingSuccess = pStreamSource->startGrabbing(pStreamSource, 0, eGrabStrategy);

	if( isStartGrabbingSuccess != 0)
	{
		printf("StartGrabbing  fail.\n");
		return -1;
	}

	return 0;
}

 int32_t GENICAM_stopGrabbing(GENICAM_StreamSource *pStreamSource)
{
	int32_t isStopGrabbingSuccess;

	isStopGrabbingSuccess = pStreamSource->stopGrabbing(pStreamSource);
	if( isStopGrabbingSuccess != 0)
	{
		printf("StopGrabbing  fail.\n");
		return -1;
	}

	return 0;
}

 int32_t setSoftTriggerConf(GENICAM_AcquisitionControl *pAcquisitionCtrl)
{
	int32_t nRet;
	GENICAM_EnumNode enumNode = {0};
	
	// 设置触发源为软触发
	enumNode = pAcquisitionCtrl->triggerSource(pAcquisitionCtrl);
	nRet = enumNode.setValueBySymbol(&enumNode, "Software");
	if (nRet != 0)
	{
		printf("set trigger source failed.\n");
		//注意：需要释放enumNode内部对象内存
		enumNode.release(&enumNode);
		return -1;
	}
	//注意：需要释放enumNode内部对象内存
	enumNode.release(&enumNode);

	// 设置触发器
	memset(&enumNode, 0, sizeof(enumNode));
	enumNode = pAcquisitionCtrl->triggerSelector(pAcquisitionCtrl);
	nRet = enumNode.setValueBySymbol(&enumNode, "FrameStart");
	if (nRet != 0)
	{
		printf("set trigger selector failed.\n");
		//注意：需要释放enumNode内部对象内存
		enumNode.release(&enumNode);
		return -1;
	}
	//注意：需要释放enumNode内部对象内存
	enumNode.release(&enumNode);

	// 设置触发模式
	memset(&enumNode, 0, sizeof(enumNode));
	enumNode = pAcquisitionCtrl->triggerMode(pAcquisitionCtrl);
	nRet = enumNode.setValueBySymbol(&enumNode, "On");
	if (nRet != 0)
	{
		printf("set trigger mode failed.\n");
		//注意：需要释放enumNode内部对象内存
		enumNode.release(&enumNode);
		return -1;
	}
	//注意：需要释放enumNode内部对象内存
	enumNode.release(&enumNode);

	return 0;
}

 int32_t executeTriggerSoftware(GENICAM_AcquisitionControl *pAcquisitionCtrl)
{
	int32_t isTriggerSoftwareSuccess;

	GENICAM_CmdNode cmdNode = pAcquisitionCtrl->triggerSoftware(pAcquisitionCtrl);
	isTriggerSoftwareSuccess = cmdNode.execute(&cmdNode);
	if(isTriggerSoftwareSuccess != 0)
	{
		printf("Execute triggerSoftware fail.\n");

		//注意：需要释放cmdNode内部对象内存
		cmdNode.release(&cmdNode);
		return -1;
	}

	//注意：需要释放cmdNode内部对象内存
	cmdNode.release(&cmdNode);

	return 0;
}

 int32_t GENICAM_disconnect(GENICAM_Camera *pGetCamera)
{
	int32_t isDisconnectSuccess;

	isDisconnectSuccess = pGetCamera->disConnect(pGetCamera);
	if( isDisconnectSuccess != 0)
	{
		printf("disconnect fail.\n");
		return -1;
	}
	
	return 0;
}

void onGetFrame(GENICAM_Frame* pFrame)
{
	int32_t ret = -1;
	uint64_t blockId = 0;

	// 标准输出换行
	printf("\r\n");

	ret = pFrame->valid(pFrame);
	if(0 == ret)
	{
		blockId = pFrame->getBlockId(pFrame);
		printf("blockId = %d.\r\n",blockId);
		isGrabbingFlag = 0;
	}
	else
	{
		printf("Frame is invalid!\n");
	}

	//注意：不管帧是否有效，都需要释放pFrame内部对象内存
	ret = pFrame->release(pFrame);

	return;
}

// ********************** 这部分处理与SDK操作相机无关，用于显示设备列表 begin*****************************
 void displayDeviceInfo(GENICAM_Camera *pCameraList, int cameraCnt)
{
	GENICAM_Camera *pDisplayCamera = NULL;
	GENICAM_GigECameraInfo GigECameraInfo;
	GENICAM_GigECamera* pGigECamera = NULL;
	int cameraIndex;
	int keyType;
	const char *vendorName = NULL;
	char vendorNameCat[11];
	const char* deviceUserID = NULL;
	char deviceUserIDCat[16];
	const char* ipAddress = NULL;

	int ret = 0;

	/* 打印Title行 */
	printf("\nIdx Type Vendor     Model      S/N             DeviceUserID    IP Address    \n");
	printf("------------------------------------------------------------------------------\n");

	for (cameraIndex = 0; cameraIndex < cameraCnt; cameraIndex++)
	{
		pDisplayCamera = &pCameraList[cameraIndex];
		/* Idx 设备列表的相机索引 最大表示字数：3*/
		printf("%-3d", cameraIndex + 1);

		/* Type 相机的设备类型（GigE，U3V，CL，PCIe）*/
		keyType = pDisplayCamera->getType(pDisplayCamera);
		switch (keyType)
		{
		case typeGige:
			printf(" GigE");
			break;
		case typeUsb3:
			printf(" U3V ");
			break;
		case typeCL:
			printf(" CL  ");
			break;
		case typePCIe:
			printf(" PCIe");
			break;
		default:
			printf("     ");
			break;
		}

		/* VendorName 制造商信息  最大表示字数：10 */
		vendorName = pDisplayCamera->getVendorName(pDisplayCamera);
		if (strlen(vendorName) > 10)
		{
			strncpy(vendorNameCat, vendorName, 7);
			vendorNameCat[7] = '\0';
			strcat(vendorNameCat, "...");
			printf(" %-10.10s", vendorNameCat);
		}
		else
		{
			printf(" %-10.10s", vendorName);
		}

		/* ModeName 相机的型号信息 最大表示字数：10 */
		printf(" %-10.10s", pDisplayCamera->getModelName(pDisplayCamera));

		/* Serial Number 相机的序列号 最大表示字数：15 */
		printf(" %-15.15s", pDisplayCamera->getSerialNumber(pDisplayCamera));

		/* deviceUserID 自定义用户ID 最大表示字数：15 */
		deviceUserID = pDisplayCamera->getName(pDisplayCamera);
		if (strlen(deviceUserID) > 15)
		{
			strncpy(deviceUserIDCat, deviceUserID, 12);
			deviceUserIDCat[12] = '\0';
			strcat(deviceUserIDCat, "...");
			printf(" %-15.15s", deviceUserIDCat);
		}
		else
		{
			memcpy(deviceUserIDCat, deviceUserID, sizeof(deviceUserIDCat));
			printf(" %-15.15s", deviceUserIDCat);
		}

		/* IPAddress GigE相机时获取IP地址 */
		if (keyType == typeGige)
		{
			GigECameraInfo.pCamera = pDisplayCamera;
			ret = GENICAM_createGigECamera(&GigECameraInfo, &pGigECamera);
			if (ret == 0)
			{
				ipAddress = pGigECamera->getIpAddress(pGigECamera);
				if (ipAddress != NULL)
				{
					printf(" %s", pGigECamera->getIpAddress(pGigECamera));
				}

			}
		}

		printf("\n");
	}
	return;
}

 char* trim(char* pStr)
{
	char* pDst = pStr;
	char* pTemStr = NULL;
	int ret = -1;

	if (pDst != NULL)
	{
		pTemStr = pDst + strlen(pStr) - 1;
		//除去字符串首部空格
		while ( *pDst ==' ')
		{
			pDst++;
		}
		//除去字符串尾部空格
		while ((pTemStr > pDst) &&(*pTemStr == ' '))
		{
			*pTemStr-- = '\0';
		}
	}
	return pDst;
}


 int isInputValid(char* pInpuStr)
{
	char numChar;
	char* pStr = pInpuStr;
	while (*pStr != '\0')
	{
		numChar = *pStr;
		if ((numChar > '9') || (numChar < '0'))
		{
			return -1;
		}
		pStr++;
	}
	return 0;
}


 int selectDevice(int cameraCnt)
{
	char inputStr[256] = {0};
	char* pTrimStr;
	int inputIndex = -1;
	int ret = -1;
	/* 提示用户选择 */
	printf("\nPlease input the camera index: ");
	while (1)
	{
		/* 获取输入内容字符串 */
		memset(inputStr, 0, sizeof(inputStr));
		scanf("%s", inputStr);
		/* 清空输入缓存 */
		fflush(stdin);

		/*除去字符串首尾空格 */
		pTrimStr = trim(inputStr);
		//判断输入字符串是否为数字
		ret = isInputValid(pTrimStr);
		if (ret == 0)
		{
			/* 输入的字符串转换成为数字 */
			inputIndex = atoi(pTrimStr);
			/* 判断用户选择合法性 */
			inputIndex -= 1;//显示的Index是从1开始
			if ((inputIndex >= 0) && (inputIndex < cameraCnt))
			{
				break;
			}
		}

		printf("Input invalid! Please input the camera index: ");
	}
	return inputIndex;
}
// ********************** 这部分处理与SDK操作相机无关，用于显示设备列表 end*****************************
